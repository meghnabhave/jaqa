import numpy as np
import json
import math
from bisect import insort_left
from multiprocessing.pool import ThreadPool
from functools import partial
from itertools import chain
from pprint import pprint

#
#   Utility Functions for Retriever
#

# Converts list of lists to single list
# Flatten one level of nesting
def flatten(listOfLists):
    return list(chain.from_iterable(listOfLists))


#
# Input: count_matrix : initialised to 0.0 (all values)
#        docs_in_batch : documents to be added in count_matrix
#        term_dict : vocabulary of corpus in lexicographical order
# Output: count_matrix[d][t] = frequency of term t in document d
def build_count_matrix(count_matrix, docs_in_batch ,term_dict):
    for docs in docs_in_batch:

        for doc_id, doc in docs.items():
            if(doc_id != 'Title'):
                for para_id, para in doc.items():
                    for term,freq in para["tokens"].items():
                        if(term in term_dict):
                            count_matrix[term_dict.index(term),int(doc_id)] = count_matrix[term_dict.index(term),int(doc_id)] + int(freq)
    return count_matrix

#
# Input : count_matrix[d][t] = frequency of term t in document d
# Output : tfidf -> dictionary with key as d (doc_id) and value as the document vector of document d
def build_tfidf_matrix(count_matrix):
    total_docs = count_matrix.shape[1]
    tfidf = np.copy(count_matrix)

    for (x,y), value in np.ndenumerate(tfidf):
        # idf = log(total number of documents in corpus / number of documents where term t appears)
        idf = math.log((total_docs+0.5)/(np.count_nonzero(count_matrix[x].astype(int))+0.5))
        # tfidf = term_frequency * idf
        tfidf[x,y] = tfidf[x,y] * idf

    # Create dictionary for hashing and swifter access
    tfidf_dict = {}
    i = 0;
    for doc_vector in tfidf.T:
        tfidf_dict[i] = list(doc_vector)
        i = i+1

    return tfidf_dict


#
# Input: tfidf_matrix : Dictionary with key as d (doc_id) and value as document vector of document d
#         query_vector : query vector of the query
# Output: sorted_doc_scores : top 5  ( doc_id,doc_scores ) sorted in order of doc_scores
def calculate_doc_scores( tfidf_matrix, query_vector):
    sqrt_mod_query = math.sqrt(np.dot(query_vector,query_vector)) + 0.5
    doc_scores = {}
    for doc_id,doc_vector in tfidf_matrix.items():
        doc_scores[doc_id] = -1 * (np.dot(doc_vector,query_vector)/(math.sqrt(np.dot(doc_vector,doc_vector) + 0.5) * sqrt_mod_query))

    sorted_doc_scores = sorted(doc_scores.items(), key=lambda x: x[1])
    return sorted_doc_scores[0:5]

#
# Input: tfidf_matrix : Dictionary with key as d (doc_id) and value as document vector of document d
#         query_vector : query vector of the query
# Output: doc_scores : array of tuple(doc_id, doc_scores) sorted in order of doc_scores
def get_closest_docs(tfidf_matrix, query_vector):
    num_of_threads=2
    step = max(int(len(tfidf_matrix) / num_of_threads), 1)
    # doc_ids_batches=[range(i,i+step) for i in range(0,tfidf_matrix.shape[1], step)]
    batches = [dict(list(tfidf_matrix.items())[i:i + step]) for i in range(0, len(tfidf_matrix), step)]
    with ThreadPool(num_of_threads) as threads:
        closest_docs = partial(calculate_doc_scores,query_vector=query_vector)
        doc_scores=threads.map(closest_docs,batches)
    doc_scores=flatten(doc_scores)
    doc_scores.sort(key=lambda x: x[1])
    return(doc_scores)

#
# Input: query_dict : Dictionary with key as word in the query and value as frequency of word in the query_word
#        term_dict : vocabulary of corpus in lexicographical order
# Output: query_vector[t] = frequency of term t in query
def build_query_vector(query_dict,term_dict):

    query_vector = [0]*len(term_dict)
    for query_word,freq in query_dict.items():
        if(query_word in term_dict):
            query_vector[term_dict.index(query_word)] = freq

    return query_vector
