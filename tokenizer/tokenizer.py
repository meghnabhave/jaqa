
import json
from collections import OrderedDict
import pprint

from keras.preprocessing.text import Tokenizer

from utils_and_constants import constants

global_word_count = []       #An array of dicts which has "term":count as key value pair

global_dictionary = set()       #global dictionary of all words across all documents
total_docs = 48
stopwords = set()
with open(constants.PROJECT_HOME+ 'tokenizer/stopwords.txt') as f:
    line = f.readline()
    s = line.split(',')
    for elem in s:
        stopwords.add(elem)

def tokenize(flag, question = None) :
    if flag == "question":
        q = list()
        q.append(question)
        t = Tokenizer()
        t.fit_on_texts(q)
        d = dict(t.word_counts)
        tem = d.copy()
        for key in tem:
            if key in stopwords:
                del d[key]
        return d

    else:
        for i in range(0, total_docs):
            local_dictionary = []
            file_name = constants.PROJECT_HOME + "knowledge_base/" + str(i) + ".txt"
            # fo = open(file_name, "r")
            with open(file_name, 'r') as json_file:
                fo = json.load(json_file)
            docs = fo["Content"].strip()
            docs = [s.strip() for s in docs.splitlines() ]
            # print(docs)
            paragraph_dict = dict()

            no_of_paragraphs = len(docs)
            for j in range(0, no_of_paragraphs):
                paragraph_details = dict()
                lines_in_para = docs[j].split('<stop>')
                paragraph_details["text"] = lines_in_para
                t = Tokenizer()
                t.fit_on_texts(lines_in_para)
                d = dict(t.word_counts)
                tem = d.copy()
                for key in tem:
                    if key in stopwords:
                        del d[key]
                paragraph_details["tokens"] = d
                paragraph_dict[j] = paragraph_details

            x = dict()
            x["Title"] = fo["Title"].strip()
            x[i] = paragraph_dict

            global_word_count.append(x)
            t = Tokenizer()
            t.fit_on_texts(docs)
            local_dictionary = [w for w in dict(t.word_docs).keys() if not w in stopwords]
            global_dictionary.update(local_dictionary)
        with open(constants.PROJECT_HOME+ 'global_data/term_dict', 'w') as file:
            lst = map(str,global_dictionary)
            line = ",".join(lst)
            file.write(line)

        with open(constants.PROJECT_HOME+'global_data/term_count.json', 'w') as file:
             file.write(json.dumps(global_word_count))


# if __name__ == '__main__':
#     d = tokenize('doc')
