from multiprocessing import Pool
import json
from itertools import repeat
import argparse
import sys

import numpy as np
from math import ceil
from pprint import pprint

from utils_and_constants.utils import build_count_matrix, build_tfidf_matrix
from utils_and_constants import constants

from tokenizer.tokenizer import tokenize

from preprocess.train_models.question_classifier import train_question_classifier
from preprocess.train_models.supervised_sentence_ranker import train_sentence_ranker



if __name__ == '__main__':

    # tokenize documents in corpus and creates files term_count.json and term_dict.txt
    tokenize("doc")

    with open(constants.PROJECT_HOME + 'global_data/term_count.json') as json_file:
        tokenizer_output = json.load(json_file)
    with open(constants.PROJECT_HOME + 'global_data/term_dict') as json_file:
        term_dict = json_file.read().split(",")


    #calculate number of processes dynamically depending on number of documents
    number_of_documents_in_a_thread = 10
    total_docs = len(tokenizer_output)
    number_of_processes = int(total_docs/number_of_documents_in_a_thread) + 1

    vocab_size = len(term_dict)

    # Count Matrix Creation
    count_matrix = np.zeros((vocab_size,total_docs))
    pool = Pool(processes=number_of_processes)              # start worker processes

    step = max(number_of_documents_in_a_thread, 1)
    batches = [tokenizer_output[i:i + step] for i in range(0, total_docs, step)]

    for c in pool.starmap(build_count_matrix,zip(repeat(count_matrix),batches,repeat(term_dict))):
        count_matrix=count_matrix+c

    pool.close()
    pool.join()

    # TFIDF Dictionary Creation
    tfidf = build_tfidf_matrix(count_matrix)

    # Write TFIDF Dictionary to file
    json = json.dumps(tfidf)
    f = open(constants.PROJECT_HOME+"global_data/tfidf.json","w")
    f.write(json)
    f.close()

    #PARSE Arguments
    parser = argparse.ArgumentParser()
    group1 = parser.add_mutually_exclusive_group()
    group1.add_argument("-s", "--supervised",help="supervised training", action="store_true")
    group1.add_argument("-u", "--unsupervised", help="unsupervised training", action="store_true")

    group2 = parser.add_mutually_exclusive_group()
    group2.add_argument("-infersent", "--infersent",help="InferSent sentence encoding", action="store_true")
    group2.add_argument("-unisent", "--unisent", help="Universal sentence encoding", action="store_true")

    learning_method = ""
    encoding_scheme = ""

    args = parser.parse_args()
    if len(sys.argv) != 2:
    	learning_method = "unsupervised"
    	encoding_scheme = "infersent"
    else:
    	if args.supervised:
    		learning_method = "supervised"
    		if args.infersent:
    			encoding_scheme = "infersent"
    		elif args.unisent:
    			encoding_scheme = "unisent"
    	elif args.unsupervised:
    		learning_method ="unsupervised"
    		if args.infersent:
    			encoding_scheme = "infersent"
    		elif args.unisent:
    			encoding_scheme = "unisent"


    constants.ENCODING_SCHEME = encoding_scheme
    constants.LEARNING_METHOD = learning_method

    print("\n\n\n\n************************************")
    print(constants.LEARNING_METHOD)
    print(constants.ENCODING_SCHEME)
    print("***********************************\n\n\n")


    #TRAIN

    train_question_classifier()
    train_sentence_ranker()
