
import json
import argparse
import sys
from pprint import pprint

# from corenlp import NER, kerasToCoreNLPEntityMatcher
# from question_classifier import predict_question_label

from utils_and_constants import constants
from tokenizer.tokenizer import tokenize
from retriever.retriever import retrieveTopNDocs, retrieveTopNParagraphs
from reader.sentence_ranker import retrieveTopNSentences
from answer_extraction.exact_span import get_exact_span



if __name__ == '__main__':

    with open(constants.PROJECT_HOME+'global_data/term_count.json') as json_file:
        tokenizer_output = json.load(json_file)
    with open(constants.PROJECT_HOME+'global_data/term_dict') as json_file:
        term_dict = json_file.read().split(",")
    with open(constants.PROJECT_HOME+'global_data/tfidf.json') as json_file:
        tfidf = json.load(json_file)


    parser = argparse.ArgumentParser()
    group1 = parser.add_mutually_exclusive_group()
    group1.add_argument("-s", "--supervised",help="supervised training", action="store_true")
    group1.add_argument("-u", "--unsupervised", help="unsupervised training", action="store_true")

    group2 = parser.add_mutually_exclusive_group()
    group2.add_argument("-infersent", "--infersent",help="InferSent sentence encoding", action="store_true")
    group2.add_argument("-unisent", "--unisent", help="Universal sentence encoding", action="store_true")

    learning_method = ""
    encoding_scheme = ""

    args = parser.parse_args()
    if len(sys.argv) != 2:
    	learning_method = "unsupervised"
    	encoding_scheme = "unisent"
    else:
    	if args.supervised:
    		learning_method = "supervised"
    		if args.infersent:
    			encoding_scheme = "infersent"
    		elif args.unisent:
    			encoding_scheme = "unisent"
    	elif args.unsupervised:
    		learning_method ="unsupervised"
    		if args.infersent:
    			encoding_scheme = "infersent"
    		elif args.unisent:
    			encoding_scheme = "unisent"


    constants.ENCODING_SCHEME = encoding_scheme
    constants.LEARNING_METHOD = learning_method

    print("\n\n\n********************************")
    print(constants.ENCODING_SCHEME)
    print(constants.LEARNING_METHOD)
    print("***********************************\n\n\n")


    while(True):
        print("\n\n\n\n\n\n")
        query = input("Enter a query: ")
        tokenized_query = tokenize("question",query)

        doc_scores = retrieveTopNDocs(tfidf,term_dict,tokenized_query)

        para_score_list,para_text_list = retrieveTopNParagraphs(tokenized_query, tokenizer_output, doc_scores)

        topSentences = retrieveTopNSentences(para_text_list, query)

        #pprint(topSentences)
        get_exact_span(topSentences, query)
        # answer = extractAnswer(topSentences)
