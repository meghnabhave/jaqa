from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
import sys
sys.path.insert(0, '/home/balgond/jaqa_final')
from mainUI import returnQuery
from .forms import NameForm

def get_name(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NameForm(request.POST)
        # check whether it's valid:
        if form.is_valid():

            print(form.cleaned_data['your_name'])
            print(form.cleaned_data['model'])
            print(form.cleaned_data['encoding'])
            answer=returnQuery(form.cleaned_data['encoding'],form.cleaned_data['model'],form.cleaned_data['your_name'])
            form = NameForm()
            return render(request, 'jaqa/temp.html', {'answer': answer,'form':form})

    # if a GET (or any other method) we'll create a blank form
    else:
        form = NameForm()

    return render(request, 'jaqa/temp.html', {'form': form})


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

def request_query(request):



    return HttpResponse("Hello, world.Call function here to search")
