
from utils_and_constants import constants

def retrieveTopNSentences(topParagraphs, query):

    if constants.LEARNING_METHOD == "unsupervised":
        from .unsupervised.sentence_ranker import retrieveTopNSentences
        return retrieveTopNSentences(topParagraphs, query)

    elif constants.LEARNING_METHOD == "supervised":
        from .supervised.sentence_ranker import retrieveTopNSentences
        return retrieveTopNSentences(topParagraphs,query)
