import tensorflow as tf
import tensorflow_hub as hub
import numpy as np

def embed_useT(module):
    print("\n\n\n\n**************** USE ************************\n\n\n\n")
    with tf.Graph().as_default():
        sentences = tf.placeholder(tf.string)
        embed = hub.Module(module)
        embeddings = embed(sentences)
        session = tf.train.MonitoredSession()
    return lambda x: session.run(embeddings, {sentences: x})
