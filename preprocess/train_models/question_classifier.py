import os
import tensorflow as tf
import tensorflow_hub as hub
import pandas as pd
import numpy as np
import re

import keras.layers as layers
from keras.models import Model
from keras import backend as K

from utils_and_constants import constants

module_url = constants.PROJECT_HOME+"library/UniversalSentenceEncoder"
# Import the Universal Sentence Encoder's TF Hub module
embed = hub.Module(module_url)

def UniversalEmbedding(x):
    return embed(tf.squeeze(tf.cast(x, tf.string)),
        signature="default", as_dict=True)["default"]



def get_dataframe(filename):
    lines = open(filename, 'r',encoding="latin-").read().splitlines()
    data = []
    for i in range(0, len(lines)):
        label = lines[i].split(' ')[0]
        label = label.split(":")[0]
        text = ' '.join(lines[i].split(' ')[1:])
        text = re.sub('[^A-Za-z0-9 ,\?\'\"-._\+\!/\`@=;:]+', '', text)
        data.append([label, text])

    df = pd.DataFrame(data, columns=['label', 'text'])
    df.label = df.label.astype('category')
    return df

embed_size = embed.get_output_info_dict()['default'].get_shape()[1].value



def train_question_classifier():

    df_train = get_dataframe(constants.PROJECT_HOME+'library/TREC_dataset/train_1000.txt')
    # print(df_train.head())

    category_counts = len(df_train.label.cat.categories)
    # print(category_counts)

    input_text = layers.Input(shape=(1,), dtype=tf.string)
    embedding = layers.Lambda(UniversalEmbedding, output_shape=(embed_size,))(input_text)
    dense = layers.Dense(256, activation='relu')(embedding)
    pred = layers.Dense(category_counts, activation='softmax')(dense)
    model = Model(inputs=[input_text], outputs=pred)
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    # print(model.summary())

    train_text = df_train['text'].tolist()
    train_text = np.array(train_text, dtype=object)[:, np.newaxis]
    train_label = np.asarray(pd.get_dummies(df_train.label), dtype = np.int8)

    df_test = get_dataframe(constants.PROJECT_HOME+'library/TREC_dataset/test.txt')

    test_text = df_test['text'].tolist()
    test_text = np.array(test_text, dtype=object)[:, np.newaxis]
    test_label = np.asarray(pd.get_dummies(df_test.label), dtype = np.int8)

    # print(categories)

    session = tf.Session()

    K.set_session(session)

    categories = df_train.label.cat.categories.tolist()

    print(categories)
    with open(constants.PROJECT_HOME+'global_data/question_classifier_categories.txt', 'w') as f:
        for cat in categories:
            f.write("%s," % cat)

    session.run(tf.global_variables_initializer())
    session.run(tf.tables_initializer())
    history = model.fit(train_text,
              train_label,
              validation_data=(test_text, test_label),
              epochs=10,
              batch_size=32)
    model.save_weights(constants.PROJECT_HOME+'global_data/question_classifier_model.h5')
