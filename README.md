# JAQA - Just Another Question Answering System

JAQA is a question answering system. The corpus is a host of wikipedia articles (in folder *knowledge_base*).

To run the system:

1. Setup *InferSent* in folder *library/InferSent*. [InferSent setup instructions](https://github.com/facebookresearch/InferSent)

2. Update *PROJECT_HOME* variable in *utils_and_constants/constants.py* with the path of the project directory.

```python
PROJECT_HOME = "/home/meghnabhave/BTP/jaqa/"
```

3. In folder *jaqa/*


```python3
jaqa$ python3 preprocess.py [-infersent, -unisent]

jaqa$ python3 main.py
```

## Adding a new Dataset

To use a new dataset in the system, we need :

1. articles (files with content) in *knowledge_base/*

2. Features files for training the supervised sentence ranker. Examples of these files in the current system are:
    * *global_data/features_infersent.csv*
    * *global_data/features_unisent.csv*




    |    | target   |          cosine_sim         |         euclidean_dis        |
    |----|----------|-----------------------------|------------------------------|
    | 0  |    4     |   [ 0.567 , ... , 0.678 ]   |   [ 0.456 , ... , 0.345 ]    |
    | 1  |    5     |   [ 0.124 , ... , 0.231 ]   |   [ 0.97 , ... , 0.764 ]     |





The above table is for each question in the dataset.
Here


* **target** is the index of sentence containing answer in the given context.


* **cosine_sim** is a 10-Dimensional vector where *cosine_sim[i]* = cosine similarity of sentence[i] vector in the context and the question vector


* **euclidean_dis** is a 10-Dimensional vector where *euclidean_dis[i]* = euclidean distance of sentence[i] vector in the context and the question vector


These files can be generated for SQuAD like Datasets using the script *get_SQUAD_features.py*

```python3
jaqa$ python3 get_SQUAD_features.py
```


For other dataset formats, you will have to write a script to bring the files in the above format. (Refer to *get_SQUAD_features.py* for inspiration.)


### For SQuAD-like Datasets

  The supervised reader models (for sentence ranking) are trained using SQuAD.

  To train with your own SQuAD-like dataset:

  1. Save the new dataset in *library/SQuAD/new_dataset_name*

  2. In main of *get_SQUAD_features.py*, make the following change

```python
file_name = constants.PROJECT_HOME+"library/SQUAD/new_dataset_name"
```

  3. In folder *jaqa/* run *get_SQUAD_features.py*

```python3
jaqa$ python3 get_SQUAD_features.py

Enter encoding scheme for features file: (infersent/unisent)......{your_scheme}
```
