from django.urls import path
from django.conf import settings
from . import views
from django.conf.urls.static import static
# from jaqa import views

urlpatterns = [
    path('', views.index, name='index'),
    path('query', views.get_name, name='get_name'),
    path('request_query', views.request_query, name='request_query'),
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
