# import constants

import nltk
# nltk.download('punkt')

from library.InferSent.models import InferSent
from utils_and_constants import constants
import torch

V = 2
MODEL_PATH = constants.PROJECT_HOME + 'library/InferSent/encoder/infersent%s.pickle' % V
params_model = {'bsize': 64, 'word_emb_dim': 300, 'enc_lstm_dim': 2048,
                'pool_type': 'max', 'dpout_model': 0.0, 'version': V}
infersent = InferSent(params_model)
infersent.load_state_dict(torch.load(MODEL_PATH))
W2V_PATH = constants.PROJECT_HOME + 'library/InferSent/dataset/fastText/crawl-300d-2M-subword.vec'
infersent.set_w2v_path(W2V_PATH)

infersent.build_vocab_k_words(K=10000)
with open(constants.PROJECT_HOME + 'global_data/term_dict') as json_file:
    term_dict = json_file.read().split(",")
infersent.update_vocab(term_dict)

def embed_sentences(sentences):
    # infersent.update_vocab(sentences)
    # print("\n\n\n\n**************** INFERSENT ************************\n\n\n\n")

    sentences_embeddings = infersent.encode(sentences, tokenize=True)

    return sentences_embeddings
