import json
from pprint import pprint
import numpy as np, pandas as pd
import warnings
warnings.filterwarnings('ignore')

import tensorflow as tf
import tensorflow_hub as hub
import keras.layers as layers
from keras.models import Model
from keras import backend as K


from utils_and_constants import constants
from sentence_encoding.encoding import embedding_fn

embed_fn = embedding_fn()


def get_euclidean_distance(sentences, question):

    distance = np.array((sentences - question) ** 2)
    distance = np.sum(distance, axis=1)
    distance = np.sqrt(distance)

    return distance




def retrieveTopNSentences(topParagraphs, query):

    # print("\n\n\n\n**************** SUPERVISED ************************\n\n\n\n")

    question_embedding = embed_fn([query])

    sim = []
    euc = []

    n = 10 # assumed no of sentences in a paragraph
    num_sentences = []
    sentences_embedding = np.array([])
    for topParagraph in topParagraphs:

        m = len(topParagraph)

        if(n <= m):
            last = "".join(topParagraph[:n-m])
            topParagraph = topParagraph[:n-1]
            topParagraph.append(last)

        num_sentences.append(len(topParagraph))

        sentences_embedding = embed_fn(topParagraph)
        # cosine similarity
        similarity_matrix = np.squeeze(np.inner(sentences_embedding, question_embedding))


        # euclidean distance
        euclidean_distance = get_euclidean_distance(sentences_embedding, question_embedding)


        if(len(topParagraph) < 10):
            extra = np.zeros((n - len(topParagraph),))
            similarity_matrix = np.append(similarity_matrix,extra)
            euclidean_distance = np.append(euclidean_distance,extra)

        sim.append(similarity_matrix)
        euc.append(euclidean_distance)

    sim = np.array(sim)
    euc = np.array(euc)

    with open(constants.PROJECT_HOME+'global_data/sentence_ranker_categories.txt', 'r') as f:
        categories = f.read().split(",")
    categories = list(filter(None, categories))
    categories = list(map(int, categories))

    category_counts = len(categories)


    # Model layers

    input1 = layers.Input(shape=(10,),dtype=tf.float32)

    input2 = layers.Input(shape=(10,),dtype=tf.float32)

    concat = layers.Concatenate(axis=1)([input1,input2])

    dense = layers.Dense(256, activation='relu')(concat)

    pred = layers.Dense(category_counts, activation='softmax')(dense)

    model = Model(inputs=[input1, input2], outputs=pred)

    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    session = tf.Session()
    K.set_session(session)
    session.run(tf.global_variables_initializer())
    session.run(tf.tables_initializer())
    model.load_weights(constants.PROJECT_HOME+'global_data/sentence_ranking_model_'+constants.ENCODING_SCHEME+'.h5')
    predicts = model.predict([sim,euc], batch_size=32)

    # categories = df_train.target.cat.categories.tolist()

    predict_logits = predicts.argmax(axis=1)
    predict_labels = [categories[logit] for logit in predict_logits]


    answer = []
    for i in range(len(predict_labels)):
        if(predict_labels[i] < num_sentences[i]):
            answer.append(topParagraphs[i][int(predict_labels[i])])
        else:
            answer.append("No Answer.")

    return answer
