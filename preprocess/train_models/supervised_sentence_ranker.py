import json
from pprint import pprint
import numpy as np, pandas as pd
import warnings
warnings.filterwarnings('ignore')

import tensorflow as tf
import tensorflow_hub as hub
import keras.layers as layers
from keras.models import Model
from keras import backend as K


from utils_and_constants import constants


def get_euclidean_distance(sentences, question):

    distance = np.array((sentences - question) ** 2)
    distance = np.sum(distance, axis=1)
    distance = np.sqrt(distance)

    return distance




def train_sentence_ranker():

    feature_embedding_file = "features_"+constants.ENCODING_SCHEME+'.csv'
    df = pd.read_csv(constants.PROJECT_HOME+"global_data/" + feature_embedding_file)
    df.target = df.target.astype('category')

    def makeArray(text):
        text = text[1:-1]
        return np.fromstring(text,sep=' ')

    df['cosine_sim'] = df['cosine_sim'].apply(makeArray)
    df['euclidean_dis'] = df['euclidean_dis'].apply(makeArray)


    total_data = df.shape[0]
    train = int(0.7  * total_data)

    df_train = df[0:train]
    df_test = df[train+1:]

    category_counts = len(df_train.target.cat.categories)

    train_cos = df_train['cosine_sim'].tolist()
    train_cos = np.array(train_cos, dtype=float)#[:, np.newaxis]
    # print(train_cos.shape)

    train_euc = df_train['euclidean_dis'].tolist()
    train_euc = np.array(train_euc, dtype=float)#[:, np.newaxis]
    # print(train_euc.shape)

    train_target = np.asarray(pd.get_dummies(df_train.target), dtype = np.int8)

    test_cos = df_test['cosine_sim'].tolist()
    test_cos = np.array(test_cos, dtype=float)#[:, np.newaxis]
    # print(test_cos.shape)

    test_euc = df_test['euclidean_dis'].tolist()
    test_euc = np.array(test_euc, dtype=float)#[:, np.newaxis]
    # print(test_euc.shape)

    test_target = np.asarray(pd.get_dummies(df_test.target), dtype = np.int8)


    # Model layers

    input1 = layers.Input(shape=(10,),dtype=tf.float32)

    input2 = layers.Input(shape=(10,),dtype=tf.float32)

    concat = layers.Concatenate(axis=1)([input1,input2])

    dense = layers.Dense(256, activation='relu')(concat)

    pred = layers.Dense(category_counts, activation='softmax')(dense)

    model = Model(inputs=[input1, input2], outputs=pred)

    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    session = tf.Session()
    K.set_session(session)

    categories = df_train.target.cat.categories.tolist()

    # print(categories)
    with open(constants.PROJECT_HOME+'global_data/sentence_ranker_categories.txt', 'w') as f:
        for cat in categories:
            f.write("%s," % cat)

    session.run(tf.global_variables_initializer())
    session.run(tf.tables_initializer())
    history = model.fit([train_cos,train_euc],
            train_target,
            validation_data=([test_cos,test_euc], test_target),
            epochs=20,
            batch_size=32)
    model.save_weights(constants.PROJECT_HOME+'global_data/sentence_ranking_model_'+constants.ENCODING_SCHEME+'.h5')
