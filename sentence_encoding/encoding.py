

from utils_and_constants import constants


def embedding_fn(scheme="no_scheme"):

    if(scheme == "no_scheme"):
        if constants.ENCODING_SCHEME == "infersent":
            from .infersent.encoding import embed_sentences
            return lambda sentences: embed_sentences(sentences)

        elif constants.ENCODING_SCHEME == "unisent":
            from .universal_sentence_encoding.encoding import embed_useT
            return embed_useT(constants.PROJECT_HOME + 'library/UniversalSentenceEncoder')
    else:
        if scheme == "infersent":
            from .infersent.encoding import embed_sentences
            return lambda sentences: embed_sentences(sentences)

        elif scheme == "unisent":
            from .universal_sentence_encoding.encoding import embed_useT
            return embed_useT(constants.PROJECT_HOME + 'library/UniversalSentenceEncoder')
