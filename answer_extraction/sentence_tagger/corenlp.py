from pycorenlp import StanfordCoreNLP
import pprint
nlp = StanfordCoreNLP('http://10.18.1.15:9000')


def NER(text):
    output = nlp.annotate(text, properties={
        'annotators': 'ner',
        'outputFormat': 'json'
    })

    return output
