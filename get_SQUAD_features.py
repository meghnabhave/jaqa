
import json
import numpy as np
from sentence_encoding.encoding import embedding_fn
from utils_and_constants import constants
import pandas as pd
import re

alphabets= "([A-Za-z])"
prefixes = "(Mr|St|Mrs|Ms|Dr)[.]"
suffixes = "(Inc|Ltd|Jr|Sr|Co)"
starters = "(Mr|Mrs|Ms|Dr|He\s|She\s|It\s|They\s|Their\s|Our\s|We\s|But\s|However\s|That\s|This\s|Wherever)"
acronyms = "([A-Z][.][A-Z][.](?:[A-Z][.])?)"
websites = "[.](com|net|org|io|gov)"


def split_into_sentences(text):
    text = " " + text + "  "
    # text = text.replace("\n"," ")
    text = re.sub(prefixes,"\\1<prd>",text)
    text = re.sub(websites,"<prd>\\1",text)
    if "Ph.D" in text: text = text.replace("Ph.D.","Ph<prd>D<prd>")
    text = re.sub("\s" + alphabets + "[.] "," \\1<prd> ",text)
    text = re.sub(acronyms+" "+starters,"\\1<stop> \\2",text)
    text = re.sub(alphabets + "[.]" + alphabets + "[.]" + alphabets + "[.]","\\1<prd>\\2<prd>\\3<prd>",text)
    text = re.sub(alphabets + "[.]" + alphabets + "[.]","\\1<prd>\\2<prd>",text)
    text = re.sub(" "+suffixes+"[.] "+starters," \\1<stop> \\2",text)
    text = re.sub(" "+suffixes+"[.]"," \\1<prd>",text)
    text = re.sub(" " + alphabets + "[.]"," \\1<prd>",text)
    if "”" in text: text = text.replace(".”","”.")
    if "\"" in text: text = text.replace(".\"","\".")
    if "!" in text: text = text.replace("!\"","\"!")
    if "?" in text: text = text.replace("?\"","\"?")
    text = text.replace(".",".<stop>")
    text = text.replace("?","?<stop>")
    text = text.replace("!","!<stop>")
    text = text.replace("<prd>",".")
    return text


def get_euclidean_distance(sentences, question):

    distance = np.array((sentences - question) ** 2)

    distance = np.sum(distance, axis=1)

    distance = np.sqrt(distance)

    return distance

def get_data(sentence_encoder,filename):

    # answer_start , context 	question 	text 	sentences 	quest_emb 	target
    # sent_emb 	cosine_sim 	euclidean_dis 	pred_idx_cos 	pred_idx_euc 	root_match_idx
    # root_match_idx_first

    with open(filename,'r') as data_file:
        squad = json.load(data_file)['data']


    total_data = len(squad)
    train = total_data #int(0.02 * total_data)
    # test = 0.3 * total_data
    train_data = squad[0:train]
    # test_data = squad[train+1:train+4214]

    data = []

    print("Train: {0}".format(train))


    n = 10 # assummed no of sentences in a paragraph

    i = 1

    for doc in train_data:

        dataout = dict()
        dataout['Title'] = doc['title']
        # print(data['paragraphs'])
        context = str()

        for para in doc['paragraphs']:

            context = context + split_into_sentences(para['context'])
            context = context + "\n"

            # for a single paragraph
            text = para['context']
            sentences = text.split('. ', n-1)

            num_sentences = len(sentences)
            sentences_embedding = sentence_encoder(sentences)
            qas = para['qas']

            # for first question of the paragraph
            for qa in qas:

                line = []

                question = qa['question']
                question_embedding = sentence_encoder([question])

                # cosine similarity
                similarity_matrix = np.squeeze(np.inner(sentences_embedding, question_embedding))

                # print(similarity_matrix.shape)

                # euclidean distance
                euclidean_distance = get_euclidean_distance(sentences_embedding, question_embedding)

                if(num_sentences < 10):
                    extra = np.zeros((n - num_sentences,))
                    similarity_matrix = np.append(similarity_matrix,extra)
                    euclidean_distance = np.append(euclidean_distance,extra)


                answer = qa['answers'][0]['text']
                answer_start = qa['answers'][0]['answer_start']

                # line.append(answer_start) #1
                # line.append(text) #2
                # line.append(question) #3
                # line.append(answer) #4
                # line.append(sentences) #5
                # line.append(question_embedding) #6

                cumulative = 0

                for sentence in sentences:
                    prev_cum = cumulative
                    cumulative = cumulative + len(sentence)


                    if(qa['answers'][0]['text'] in sentence):
                        # for answer_start in answer_starts:
                        if(answer_start > prev_cum and answer_start < cumulative):
                            label = sentences.index(sentence)

                line.append(label) #7
                # line.append(sentences_embedding) #8
                line.append(similarity_matrix) #9
                line.append(euclidean_distance) #10

                data.append(line)

        dataout['Content'] = context
        fileout = constants.PROJECT_HOME+'knowledge_base/' + str(i-1) + '.txt'

        with open(fileout, 'w') as outfile:
            json.dump(dataout, outfile)

        print("Doc {0} done.".format(i))
        i = i+1        # print(line)


    # answer_start , context 	question 	text 	sentences 	quest_emb 	target
    # sent_emb 	cosine_sim 	euclidean_dis 	pred_idx_cos 	pred_idx_euc 	root_match_idx
    # root_match_idx_first

    # df_train = pd.DataFrame(data, columns=['label', 'text','question'])
    # df_train = pd.DataFrame(data, columns=['answer_start','context','question','text','sentences','quest_emb','target','sent_emb','cosine_sim','euclidean_dis'])

    df_train = pd.DataFrame(data, columns=['target','cosine_sim','euclidean_dis'])
    df_train.target = df_train.target.astype('category')

    return df_train






if __name__ == '__main__':

    file_name = constants.PROJECT_HOME+"library/SQUAD/dev-v2.0.json"

    scheme = input("Enter encoding scheme for features file: (infersent/unisent)......")
    sentence_encoder = embedding_fn(scheme)
    df = get_data(sentence_encoder,file_name)
    df.to_csv(constants.PROJECT_HOME+"global_data/features_"+scheme+".csv",index=True)



    start = 0

    with open(file_name, 'r') as json_file:
        fo = json.load(json_file)

        datalist = fo['data']
        end = len(datalist)
        while start<end:
            dataout = dict()
            data = datalist[start]
            dataout['Title'] = data['title']
            # print(data['paragraphs'])
            text = str()
            for para in data['paragraphs']:
                # print(split_into_sentences(para['context']))
                text = text + split_into_sentences(para['context'])
                text = text + "\n"
            dataout['Content'] = text

            fileout = './wikidata/' + str(start) + '.txt'
            with open(fileout, 'w') as outfile:
                json.dump(dataout, outfile)

            start = int(start) + 1
