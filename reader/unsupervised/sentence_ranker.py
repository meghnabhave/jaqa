import numpy as np

from sentence_encoding.encoding import embedding_fn


def retrieveTopNSentences(para_text_list, query):
    print("\n\n\n\n**************** UNSUPERVISED ************************\n\n\n\n")

    topParaSent = list()

    n = 5 # number of paragraphs
    for i in range(0,min(n,len(para_text_list))):

        topParaSent.extend(para_text_list[i])

    embed_fn = embedding_fn()

    encoding_matrix = embed_fn(topParaSent)

    query_matrix = embed_fn([query])

    similarity_matrix = -1*np.inner(encoding_matrix, query_matrix)

    # print(similarity_matrix)

    indices = sorted(range(len(similarity_matrix)),key=similarity_matrix.__getitem__) #np.argsort(similarity_matrix)

    topSentences = list()
    for i in indices:
        index = i
        for j in range(0,min(n,len(para_text_list))):
            rem = index%len(para_text_list[j])

            if(rem == index):
                #print(para_text_list[j][index])
                topSentences.append(para_text_list[j][index])
                break
            else:
                index = index - len(para_text_list[j])

    return topSentences[0:5]
