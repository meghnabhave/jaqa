from django import forms

class NameForm(forms.Form):
    model =forms.CharField(initial="unsupervised",widget=forms.HiddenInput())
    encoding =forms.CharField(initial="unisent",widget=forms.HiddenInput())
    your_name = forms.CharField(label='',widget=forms.TextInput(attrs={'class': 'form-control','placeholder':"Search term..."}))
