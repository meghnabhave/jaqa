
import json
import argparse
import sys
from pprint import pprint

# from corenlp import NER, kerasToCoreNLPEntityMatcher
# from question_classifier import predict_question_label

from utils_and_constants import constants
from tokenizer.tokenizer import tokenize
from retriever.retriever import retrieveTopNDocs, retrieveTopNParagraphs
from reader.sentence_ranker import retrieveTopNSentences
from answer_extraction.exact_span import get_exact_span




def returnQuery(encoding_scheme,learning_method,query):

    with open(constants.PROJECT_HOME+'global_data/term_count.json') as json_file:
        tokenizer_output = json.load(json_file)
    with open(constants.PROJECT_HOME+'global_data/term_dict') as json_file:
        term_dict = json_file.read().split(",")
    with open(constants.PROJECT_HOME+'global_data/tfidf.json') as json_file:
        tfidf = json.load(json_file)
    constants.ENCODING_SCHEME = encoding_scheme
    constants.LEARNING_METHOD = learning_method
    print("\n\n\n********************************")
    print(constants.ENCODING_SCHEME)
    print(constants.LEARNING_METHOD)
    print("***********************************\n\n\n")
    tokenized_query = tokenize("question",query)

    doc_scores = retrieveTopNDocs(tfidf,term_dict,tokenized_query)

    para_score_list,para_text_list = retrieveTopNParagraphs(tokenized_query, tokenizer_output, doc_scores)

    topSentences = retrieveTopNSentences(para_text_list, query)

    #pprint(topSentences)

    answer_list=get_exact_span(topSentences, query)
    print(answer_list)
    return answer_list


if __name__ == '__main__':
    print(returnQuery('unisent','unsupervised','Where did the super Bowl 50 take place?'))
