import os
import tensorflow as tf
import tensorflow_hub as hub
import pandas as pd
import numpy as np
import re

import keras.layers as layers
from keras.models import Model
from keras import backend as K

from utils_and_constants import constants

module_url = constants.PROJECT_HOME+"library/UniversalSentenceEncoder"
# Import the Universal Sentence Encoder's TF Hub module
embed = hub.Module(module_url)
embed_size = embed.get_output_info_dict()['default'].get_shape()[1].value

def UniversalEmbedding(x):
    return embed(tf.squeeze(tf.cast(x, tf.string)),
        signature="default", as_dict=True)["default"]

with open(constants.PROJECT_HOME+'global_data/question_classifier_categories.txt', 'r') as f:
    categories = f.read().split(",")

categories = list(filter(None, categories))

category_counts = len(categories)

input_text = layers.Input(shape=(1,), dtype=tf.string)
embedding = layers.Lambda(UniversalEmbedding, output_shape=(embed_size,))(input_text)
dense = layers.Dense(256, activation='relu')(embedding)
pred = layers.Dense(category_counts, activation='softmax')(dense)
model = Model(inputs=[input_text], outputs=pred)
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

global graph
graph = tf.get_default_graph()

session = tf.Session()
K.set_session(session)
session.run(tf.global_variables_initializer())
session.run(tf.tables_initializer())


def predict_question_label(query):


    new_text = list()
    new_text.append(query)
    new_text.append(query)
    new_text.append(query)

    new_text = np.array(new_text, dtype=object)[:, np.newaxis]

    with graph.as_default():
        model.load_weights(constants.PROJECT_HOME+'global_data/question_classifier_model.h5')
        predicts = model.predict(new_text, batch_size=32)

    predict_logits = predicts.argmax(axis=1)
    predict_labels = [categories[logit] for logit in predict_logits]

    return predict_labels[0]
