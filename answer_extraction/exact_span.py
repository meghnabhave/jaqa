
from .question_classifier.question_classifier import predict_question_label
from .sentence_tagger.corenlp import NER

from pprint import pprint

# KERAS (What is the question asking for?)
    # ABBR - 'abbreviation': expression abbreviated, etc.
    # DESC - 'description and abstract concepts': manner of an action, description of sth. etc.
    # ENTY - 'entities': animals, colors, events, food, etc.
    # HUM - 'human beings': a group or organization of persons, an individual, etc.
    # LOC - 'locations': cities, countries, etc.
    # NUM - 'numeric values': postcodes, dates, speed,temperature, etc

# CORE_NLP (Entity in Answer)
    # ORDINAL - NUMBER, DATE, TIME
    # LOCATION, ORGANISATION, PERSON
    # MISC
def kerasToCoreNLPEntityMatcher(keras_entity):

    NER_entity = {
        "HUM" : ["PERSON","ORGANIZATION"],
        "LOC" : ["LOCATION","STATE_OR_PROVINCE","CITY","COUNTRY"],
        "NUM" : ["NUMBER", "DATE", "TIME","PERCENT","MONEY","DURATION","SET","ORDINAL"],
        "ENTY" : ["EMAIL","NATIONALITY","RELIGION","TITLE","URL","CAUSE_OF_DEATH","IDEOLOGY",
                    "CRIMINAL_CHARGE","MISC"],
        "DESC" : ["DESCRIPTIVE"]
    }.get(keras_entity, "ENTITY_NOT_FOUND")

    return NER_entity




def get_exact_span(topSentences, query):

    sentences = " ".join(topSentences)

    label = predict_question_label(query)
    # print("Question Label: {0}".format(label))

    ner_entity = kerasToCoreNLPEntityMatcher(label)

    ner_output = NER(sentences)
    while ner_output is None:
        time.sleep(15)


    ner_output = ner_output['sentences']
    pprint(ner_output)
    pprint(label)
    answer_list=list()


    if "DESCRIPTIVE" in ner_entity:
        answer_list.append(topSentences[0])
        # print("\nANSWER: {0}".format(topSentences[0]))
    else:
        match = False
        for ner_out in ner_output:
            for entity in ner_out['entitymentions']:
                if entity['ner'] in ner_entity:
                    match = True
                    answer_list.append(entity['text'])
                    # print("\nANSWER: {0}".format(entity['text']))

        if not match:
            answer_list.append(topSentences[0])
            # print("\nANSWER: {0}".format(topSentences[0]))
    return answer_list
